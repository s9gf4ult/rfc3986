module Rendering where

import Data.Proxy
import Data.Text ( Text )
import Data.Text.Internal.Builder
import RFC3986.Rendering
import RFC3986.Types
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.TH
import Text.PercentEncoded

groupRendering = $testGroupGenerator

p :: Proxy Text
p = Proxy


case_segments :: Assertion
case_segments =
    let seg :: [Text]
        seg = ["hello", "%World", "one two"]
        res = toLazyText $ segments
              $ map urlFromRaw seg
    in "hello/%25World/one%20two" @=? res

case_port :: Assertion
case_port = do
    let pnone = toLazyText $ renderPort p Nothing
        psome = toLazyText $ renderPort p $ Just 3000
    "" @=? pnone
    ":3000" @=? psome

case_ipv4 :: Assertion
case_ipv4 = do
    let addr = IPv4Address (127, 0, 0, 1)
        res = toLazyText $ renderIPv4Address p addr
    "127.0.0.1" @=? res

case_userInfo :: Assertion
case_userInfo = do
    let ui = map urlFromRaw
             ["user", "pass" :: Text]
        uinone = UserInfo $ take 0 ui
        uione = UserInfo $ take 1 ui
        uitwo = UserInfo ui
    "" @=? toLazyText (renderUserInfo uinone)
    "user@" @=? toLazyText (renderUserInfo uione)
    "user:pass@" @=? toLazyText (renderUserInfo uitwo)


path :: [PctEncoded Text]
path = ["one", "two" ]

case_ABEmpty :: Assertion
case_ABEmpty = do
    let pempty = renderABEmptyPath $ PathABEmpty $ take 0 path
        psome = renderABEmptyPath $ PathABEmpty path
    "" @=? pempty
    "/one/two" @=? psome

case_AbsolutePath :: Assertion
case_AbsolutePath = do
    let pempty = renderAbsolutePath $ PathAbsolute $ take 0 path
        psome = renderAbsolutePath $ PathAbsolute path
    "/" @=? pempty
    "/one/two" @=? psome

case_Rootless :: Assertion
case_Rootless = do
    let p1 = renderRootless $ PathRootless "some" $ take 0 path
        p2 = renderRootless $ PathRootless "some" path
    "some" @=? p1
    "some/one/two" @=? p2


case_Query :: Assertion
case_Query = do
    let q :: [(PctEncoded Text, Maybe (PctEncoded Text))]
        q = [ ("hello", Just "world")
            , ("one", Nothing) ]
        qempty = renderQuery $ Query $ take 0 q
        qone = renderQuery $ Query $ take 1 q
        qtwo = renderQuery $ Query q
    "" @=? qempty
    "?hello=world" @=? qone
    "?hello=world&one" @=? qtwo

case_Uri :: Assertion
case_Uri = do
    let
        hier = HPAuthority
                   (Authority
                    (UserInfo [])
                    (HRegName "haskell.org")
                    Nothing)
                   (PathABEmpty path)
        query = Query [("q", Just "val")]
        uri :: Uri Text
        uri = Uri
            { uriScheme = "http"
            , uriHierPart = hier
            , uriQuery = query
            , uriFragment = Fragment Nothing
            }
    "http://haskell.org/one/two?q=val" @=? renderUri uri
