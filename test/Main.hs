module Main where

import Data.Text ( Text )
import Data.Text.Internal.Builder
import RFC3986.Rendering
import RFC3986.Types
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.TH
import Text.PercentEncoded

import Rendering



main :: IO ()
main = defaultMain $ testGroup "Hunit"
    [ groupRendering
    ]
