module RFC3986.Types where

import Control.Applicative
import Control.DeepSeq ( NFData )
import Data.Buildable
import Data.CaseInsensitive ( CI, FoldCase, mk )
import Data.Data
import Data.Monoid
import Data.String
import Data.Word
import Text.PercentEncoded

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.CaseInsensitive as CI
import qualified Data.List as L

-- | Uri with decoded string data. Type argument may be Text or String
data Uri s = Uri
    { uriScheme   :: !(CI s)
    , uriHierPart :: !(HierPart (PctEncoded s))
    , uriQuery    :: !(Query (PctEncoded s))
    , uriFragment :: !(Fragment (PctEncoded s))
    }

-- | Authority plus path
data HierPart s
    = HPAuthority (Authority s) (PathABEmpty s)
    | HPAbsolute (PathAbsolute s)
    | HPRootless (PathRootless s)
    | HPEmpty
    deriving (Ord, Eq, Typeable)

data Authority s = Authority
    { auUserInfo :: !(UserInfo s)
    , auHost     :: !(Host s)
    , auPort     :: !(Maybe Word16)
    } deriving (Ord, Eq, Typeable)

-- | colon-separated user info. First element is the username, the tail
-- is like a password or other stuff specified by scheme. If innter list is
-- empty then user info considered empty.
newtype UserInfo s = UserInfo [s]
    deriving (Ord, Eq, Typeable, Functor)

-- | Host specification which can be either ip address or registered name
data Host s
    = HIPLiteral IPLiteral      -- ^ Parsed literal ip address
    | HIPv4Address IPv4Address  -- ^ Parsed ipv4 address
    | HRegName (CI s)     -- ^ dns host name which is case insensitive
    deriving (Ord, Eq, Typeable)

-- | Literal ip address specified
data IPLiteral =
    IPv6Address | IPvFuture --  FIXME: implement address storage
    deriving (Ord, Eq, Typeable)

-- | Sequence of four bytes parsed as decimal numbers separated by
-- dot. Text order.
newtype IPv4Address =
    IPv4Address (Word8, Word8, Word8, Word8)
    deriving (Ord, Eq, Typeable)


-- | Absolute or empty path. Starts from \"/\" or empty at all
newtype PathABEmpty s =
    PathABEmpty [s]
    deriving (Functor, Ord, Eq, Typeable)

-- | Path starting from \"/\" but not from \"//\", contains \"/\" even
-- if empty
newtype PathAbsolute s = PathAbsolute [s]
    deriving (Functor, Ord, Eq, Typeable)

-- | Non-empty path where first segment is non-empty and do not
-- contains colon
data PathNoScheme s = PathNoScheme s [s]
    deriving (Functor, Ord, Eq, Typeable)

-- | Non-empty path where first segment is non-empty
data PathRootless s = PathRootless s [s]
    deriving (Functor, Ord, Eq, Typeable)

-- | List of query parameters
newtype Query s = Query [(s, Maybe s)]
    deriving (Functor, Ord, Eq, Typeable)

newtype Fragment s = Fragment (Maybe s)
    deriving (Functor, Ord, Eq, Typeable)
