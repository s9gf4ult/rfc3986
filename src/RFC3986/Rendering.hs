module RFC3986.Rendering where

import RFC3986.Types

import Data.Buildable
import Data.CaseInsensitive
    ( original )
import Data.Monoid
import Data.String
import Data.Word
import Data.Proxy
import Text.PercentEncoded

import qualified Data.List as L



-- | Glues parts of URI to one string.
renderUri :: forall s. (IsString s, Buildable s)
          => Uri s
          -> Builder s
renderUri Uri{..} =
    (fromValue $ original uriScheme)
    <> (fromValue (":" :: s))
    <> renderHierPart uriHierPart
    <> renderQuery uriQuery
    <> renderFragment uriFragment

renderQuery :: forall s. (IsString s, Buildable s)
            => Query (PctEncoded s)
            -> Builder s
renderQuery (Query []) = mempty
renderQuery (Query s) =
    let params = L.intersperse (enc "&")
                 $ map qmap s
    in enc "?" <> mconcat params --  FIXME: what if all params are empty?
  where
    qmap (a, Nothing) = fromValue $ getEncoded a
    qmap (a, Just b) = mconcat $ map enc
                       [ getEncoded a , "=" , getEncoded b ]
    enc :: s -> Builder s
    enc = fromValue

renderFragment :: forall s. (IsString s, Buildable s)
               => Fragment (PctEncoded s)
               -> Builder s
renderFragment (Fragment Nothing) = mempty
renderFragment (Fragment (Just s)) = (fromValue ("#" :: s)) <> fromValue (getEncoded s)

renderHierPart :: forall s. (IsString s, Buildable s)
               => (HierPart (PctEncoded s))
               -> Builder s
renderHierPart (HPAuthority a p) =
    (fromValue ("//" :: s)) <> renderAuthority a <> renderABEmptyPath p
renderHierPart (HPAbsolute abs) = renderAbsolutePath abs
renderHierPart (HPRootless rootles) = renderRootless rootles
renderHierPart HPEmpty = mempty

segments :: (IsString s, Buildable s)
         => [PctEncoded s]
         -> Builder s
segments s = mconcat
             $ map fromValue
             $ L.intersperse "/"
             $ map getEncoded s

renderAuthority :: forall s. (IsString s, Buildable s)
                => (Authority (PctEncoded s))
                -> Builder s
renderAuthority Authority{..} =
    renderUserInfo auUserInfo
    <> renderHost auHost
    <> renderPort (Proxy :: Proxy s) auPort

renderUserInfo :: forall s. (IsString s, Buildable s)
               => UserInfo (PctEncoded s)
               -> Builder s
renderUserInfo (UserInfo []) = mempty
renderUserInfo (UserInfo uicomps) =
        let c = mconcat
                $ map fromValue
                $ L.intersperse ":"
                $ map getEncoded uicomps
        in c <> (fromValue ("@" :: s))

renderPort :: forall s. (IsString s, Buildable s)
           => Proxy s
           -> Maybe Word16
           -> Builder s
renderPort _ Nothing = mempty
renderPort _ (Just port) =
    let p :: s
        p = fromString $ show port
    in (fromValue (":" :: s)) <> fromValue p

renderHost :: forall s. (IsString s, Buildable s)
              => Host (PctEncoded s) -> Builder s
renderHost (HIPLiteral lit)  = renderIPLiteral lit
renderHost (HIPv4Address v4) = renderIPv4Address (Proxy :: Proxy s) v4
renderHost (HRegName reg)    = fromValue $ getEncoded $ original reg

renderIPLiteral = undefined     --  FIXME: implement later

renderIPv4Address :: forall s. (IsString s, Buildable s)
                  => Proxy s
                  -> IPv4Address
                  -> Builder s
renderIPv4Address _ (IPv4Address (a, b, c, d)) =
    let addr :: [s]
        addr = map (fromString . show)
               [a, b, c, d]
    in mconcat
       $ map fromValue
       $ L.intersperse "." addr


renderABEmptyPath :: forall s. (IsString s, Buildable s)
                  => (PathABEmpty (PctEncoded s)) -> Builder s
renderABEmptyPath (PathABEmpty []) = mempty
renderABEmptyPath (PathABEmpty s) =
    (fromValue ("/" :: s)) <> segments s

renderAbsolutePath :: forall s. (IsString s, Buildable s)
                   => PathAbsolute (PctEncoded s)
                   -> Builder s
renderAbsolutePath (PathAbsolute s) =
    fromValue ("/" :: s) <> segments s

renderRootless :: (IsString s, Buildable s)
               => (PathRootless (PctEncoded s))
               -> Builder s
renderRootless (PathRootless a b) = segments (a:b)
