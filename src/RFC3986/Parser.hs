module RFC3986.Parser where

import Control.Applicative
import Control.Monad
import Data.Attoparsec.ByteString.Char8
import Data.ByteString ( ByteString )
import Data.CaseInsensitive
import Data.Foldable ( toList )
import Data.Monoid
import RFC3986.Types
import Text.PercentEncoded

import qualified Data.ByteString.Char8 as BC

alpha :: Parser Char
alpha = satisfy (`elem` (['a'..'z'] ++ ['A'..'Z']))

hexDigit :: Parser Char
hexDigit =
    satisfy (`elem` (['a'..'f'] ++
                     ['A'..'F'] ++
                     ['0'..'9']))

subDelim :: Parser Char
subDelim = satisfy (`elem` "!$&'()*+,;=")

parserUri :: Parser (Uri ByteString)
parserUri = do
    uriScheme <- parserScheme
    _ <- char ':'
    uriHierPart <- parserHierPart
    uriQuery <- parserQuery
    uriFragment <- parserFragment
    return $ Uri{..}

parserScheme :: Parser (CI ByteString)
parserScheme = do
    a <- alpha
    rest <- many $ choice
        [ alpha
        , digit
        , satisfy (`elem` "+-.")
        ]
    return $ mk $ BC.pack $ a:rest

parserHierPart :: Parser (HierPart (PctEncoded ByteString))
parserHierPart = choice
    [ string "//"
      *> (HPAuthority <$> parserAuthority <*> parserABEmpty)
    , HPAbsolute <$> parserAbsolute
    , HPRootless <$> parserRootless
    , pure HPEmpty
    ]


parserAuthority :: Parser (Authority (PctEncoded ByteString))
parserAuthority = undefined

parserPathSeg :: Parser (PctEncoded ByteString)
parserPathSeg =
    fmap (fromEncoded . mconcat) $ many $ pPChar

parserPathSegNz :: Parser (PctEncoded ByteString)
parserPathSegNz =
    fmap (fromEncoded . mconcat) $ many1 $ pPChar

pPChar :: Parser ByteString
pPChar = choice
    [ pUnreserved
    , pPctEncoded
    , BC.singleton <$> subDelim
    , string ":"
    , string "@"
    ]

pUnreserved :: Parser ByteString
pUnreserved = choice
    [ BC.singleton <$> alpha
    , BC.singleton <$> digit
    , BC.singleton <$> satisfy (`elem` "-._~")
    ]

pPctEncoded :: Parser ByteString
pPctEncoded = fmap BC.pack $ sequence
    [ char '%'
    , hexDigit
    , hexDigit
    ]


parserABEmpty :: Parser (PathABEmpty (PctEncoded ByteString))
parserABEmpty = do
    _ <- char '/'
    fmap PathABEmpty $ sepBy parserPathSeg (char '/')

parserAbsolute :: Parser (PathAbsolute (PctEncoded ByteString))
parserAbsolute = do
    _ <- char '/'
    r <- optional $ do
        snz <- parserPathSegNz
        rest <- many $ do
            _ <- char '/'
            parserPathSeg
        return $ snz:rest
    return $ PathAbsolute $ maybe [] id r

parserRootless :: Parser (PathRootless (PctEncoded ByteString))
parserRootless = do
    snz <- parserPathSegNz
    rest <- many $ do
        _ <- char '/'
        parserPathSeg
    return $ PathRootless snz rest

parserQuery :: Parser (Query (PctEncoded ByteString))
parserQuery = undefined

parserFragment :: Parser (Fragment (PctEncoded ByteString))
parserFragment = undefined
